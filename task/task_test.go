package task

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetTasksIndexes(t *testing.T) {
	exp := map[string]int{
		"task-1": 0,
		"task-2": 1,
	}
	testData := []Task{
		{
			Name: "task-1",
		},
		{
			Name: "task-2",
		},
	}
	actual := GetTasksIndexes(testData)
	assert.Equal(t, actual, exp, "they should be equal")
}

func TestGetTasksIndexesWithIncorrectOrder(t *testing.T) {
	exp := map[string]int{
		"task-1": 0,
		"task-2": 2,
	}
	testData := []Task{
		{
			Name: "task-1",
		},
		{
			Name: "task-2",
		},
	}
	actual := GetTasksIndexes(testData)
	assert.NotEqual(t, actual, exp, "they should not be equal")
}

func TestSortingTasksSlice(t *testing.T) {
	exp := []Task{
		{
			Name:     "task-1",
			Command:  "cat /tmp/file1",
			Requires: nil,
		},
		{
			Name:    "task-2",
			Command: "rm /tmp/file1",
			Requires: []string{
				"task-1",
			},
		},
	}
	valueIndexes := map[string]int{"task-1": 0, "task-2": 1}
	testData := []Task{
		{
			Name:    "task-2",
			Command: "rm /tmp/file1",
			Requires: []string{
				"task-1",
			},
		},
		{
			Name:     "task-1",
			Command:  "cat /tmp/file1",
			Requires: nil,
		},
	}
	actual := SortingTasksSlice(testData, valueIndexes)
	assert.Equal(t, actual, exp, "they should be equal")
}

func TestSortingTasksSliceWithIncorrectOrder(t *testing.T) {
	exp := []Task{
		{
			Name:    "task-2",
			Command: "rm /tmp/file1",
			Requires: []string{
				"task-1",
			},
		},
		{
			Name:     "task-1",
			Command:  "cat /tmp/file1",
			Requires: nil,
		},
	}
	valueIndexes := map[string]int{"task-1": 0, "task-2": 1}
	testData := []Task{
		{
			Name:    "task-2",
			Command: "rm /tmp/file1",
			Requires: []string{
				"task-1",
			},
		},
		{
			Name:     "task-1",
			Command:  "cat /tmp/file1",
			Requires: nil,
		},
	}
	actual := SortingTasksSlice(testData, valueIndexes)
	assert.NotEqual(t, actual, exp, "they should not be equal")
}
