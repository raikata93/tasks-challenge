package task

type Tasks struct {
	Tasks []Task `json:"tasks"`
}

type Task struct {
	Name     string   `json:"name"`
	Command  string   `json:"command"`
	Requires []string `json:"requires"`
}

func GetTasksIndexes(tasks []Task) map[string]int {
	valueIndexes := make(map[string]int)
	for index, task := range tasks {
		valueIndexes[task.Name] = index
	}

	return valueIndexes
}

func SortingTasksSlice(tasks []Task, valueIndexes map[string]int) []Task {
	for _, task := range tasks {
		if len(task.Requires) > 0 {
			for _, requiredTask := range task.Requires {
				indexToRemove := valueIndexes[requiredTask]
				indexToInsert := valueIndexes[task.Name]
				valOfIndexToRemove := tasks[indexToRemove]
				tasks = append(tasks[:indexToRemove], tasks[indexToRemove+1:]...)
				newTaskSlice := make([]Task, indexToInsert+1)
				copy(newTaskSlice, tasks[:indexToInsert])
				newTaskSlice[indexToInsert] = valOfIndexToRemove
				tasks = append(newTaskSlice, tasks[indexToInsert:]...)
			}
		}
	}

	return tasks
}
