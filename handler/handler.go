package handler

import (
	"SumUpChallenge/file"
	"SumUpChallenge/task"
	"fmt"
	"net/http"
)

func tasksHandler(w http.ResponseWriter, r *http.Request) {
	tasks, err := file.ProcessJsonFileData(r)
	if err != nil {
		fmt.Fprintln(w, err)
	} else {
		sortedTasks := task.SortingTasksSlice(tasks, task.GetTasksIndexes(tasks))
		if len(sortedTasks) > 0 {
			fmt.Fprintln(w, "#!/usr/bin/env bash")
			fmt.Fprintln(w, "")
			for _, sortedTask := range sortedTasks {
				fmt.Fprintln(w, sortedTask.Command)
			}
		}
	}
}
func HandleRequests() {
	http.HandleFunc("/", tasksHandler)
	http.ListenAndServe(":4000", nil)
}
