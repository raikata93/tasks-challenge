package handler

import (
	"io/ioutil"
	"log"
	"net/http/httptest"
	"os"
	"testing"
)

func TestTasksHandler(t *testing.T) {
	requestBody, err := os.Open("testdata/requestBody.json")
	if err != nil {
		log.Fatal(err)
	}
	req := httptest.NewRequest("POST", "http://localhost:4000", requestBody)
	w := httptest.NewRecorder()
	tasksHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	actualData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	expectedResponse, err := os.ReadFile("testdata/response.txt")
	if err != nil {
		log.Fatal(err)
	}

	if string(actualData) != string(expectedResponse) {
		t.Errorf("expected %v got %v", string(expectedResponse), string(actualData))
	}
}
