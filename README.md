# SumUp Challenge 

Please follow the below steps to get started.

## How to run the tasks

1. go run main.go - this will open endpoint which listen on port 4000 
2. curl -d @requestBody.json http://localhost:4000/ - requestBody.json and requestBody2.json are already added json file to repository, but it works with all jsons

## Running tests

Unit tests can be executed using `go test ./...`.

### Developer
Martin Raychev Ivanov - raichev93@gmail.com
