package file

import (
	"SumUpChallenge/task"
	"github.com/stretchr/testify/assert"
	"net/http"
	"os"
	"testing"
)

func TestProcessJsonFileData(t *testing.T) {
	exp := []task.Task{
		{
			Name:     "task-1",
			Command:  "touch /tmp/file1",
			Requires: nil,
		},
		{
			Name:    "task-2",
			Command: "cat /tmp/file1",
			Requires: []string{
				"task-3",
			},
		},
	}
	requestBody, _ := os.Open("testdata/requestBody.json")
	req := http.Request{Body: requestBody}
	tasksFromJson, _ := ProcessJsonFileData(&req)
	assert.Equal(t, exp, tasksFromJson, "they should be equal")
}

func TestProcessJsonFileDataWithWrongRequestBodyPath(t *testing.T) {
	_, err := os.Open("testdata/requestBody123.json")
	if err == nil {
		t.Error("expected error but got nil")
	}
}

func TestProcessJsonFileDataWithInvalidJsonFile(t *testing.T) {
	requestBody, _ := os.Open("testdata/requestBodyInvalidJson.json")
	req := http.Request{Body: requestBody}
	_, err := ProcessJsonFileData(&req)
	if err == nil {
		t.Error("expected error but got nil")
	}
}
