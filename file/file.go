package file

import (
	"SumUpChallenge/task"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

var (
	errorReadingResponseBody = errors.New("unexpected error during response body reading")
	errorDuringUnmarshalJson = errors.New("unexpected error during unmarshal json")
)

func ProcessJsonFileData(r *http.Request) ([]task.Task, error) {
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, errorReadingResponseBody
	}
	var tasks task.Tasks
	err = json.Unmarshal(reqBody, &tasks)
	if err != nil {
		return nil, errorDuringUnmarshalJson
	}

	return tasks.Tasks, nil
}
